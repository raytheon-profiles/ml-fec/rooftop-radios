#!/usr/bin/python

"""
Allocate rooftop and/or fixed-endpoint nodes with associated 
compute nodes with gnuradio installed.  To see what radios are available,
and what features/restrictions they have, please check the map located here:

  [POWDER map](https://powderwireless.net/map)

Instructions:

Be sure to select at least one node/radio, or nothing  will be allocated!

Rooftop resources (Cellular and/or CBRS) are allocated as distinct pairs of 
compute and radio devices (NI X310 or N310), with a link (10G Ethernet) between 
them.

Fixed endpoint resources are allocated as single (small form factor)
compute devices, with the associated radios directly attached (e.g., NUC 
compute with an NI B210 radio connected via USB 3).

If you plan to transmit anything, you will need to declare the frequency range
you plan to use for transmission via the parameters in this profile.  If you
transmit without declaring frequencies, this will be detected, and your
experiment may be shut down.  Please make sure the frequencies you declare
are compatible with the radios you specify for allocation (check details on
the POWDER map linked in the profile description).

"""

import geni.portal as portal
import geni.rspec.pg as rspec
import geni.rspec.emulab.pnext as pn
import geni.rspec.emulab.spectrum as spectrum
import geni.rspec.emulab.route as route
import geni.rspec.igext as ig

x310_node_disk_image = \
        "urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU22-64-GR310"


# List of CBRS rooftop X310 radios.
cbrs_radios = [
    ("cbrssdr1-bes",
     "Behavioral"),
    ("cbrssdr1-browning",
     "Browning"),
    ("cbrssdr1-dentistry",
     "Dentistry"),
    ("cbrssdr1-fm",
     "Friendship Manor"),
    ("cbrssdr1-hospital",
     "Hospital"),
    ("cbrssdr1-honors",
     "Honors"),
    ("cbrssdr1-meb",
     "MEB"),
    ("cbrssdr1-smt",
     "SMT"),
    ("cbrssdr1-ustar",
     "USTAR"),
]


portal.context.defineParameter("x310_comp_nodetype",
                               "Type of the node paired with the X310 Radios",
                               portal.ParameterType.STRING, "d430", ("d430", "d740", "d710", "dl360"))

# Frequency ranges to declare for this experiment
portal.context.defineStructParameter(
    "freq_ranges", "Frequency Ranges To Transmit In", [],
    multiValue=True,
    min=0,
    multiValueTitle="Frequency ranges to be used for transmission.",
    members=[
        portal.Parameter(
            "freq_min",
            "Frequency Range Min",
            portal.ParameterType.BANDWIDTH,
            3550.0,
            longDescription="Values are rounded to the nearest kilohertz."
        ),
        portal.Parameter(
            "freq_max",
            "Frequency Range Max",
            portal.ParameterType.BANDWIDTH,
            3560.0,
            longDescription="Values are rounded to the nearest kilohertz."
        ),
    ])

# Set of CBRS X310 radios to allocate
portal.context.defineStructParameter(
    "cbrs_radio_sites", "Rooftop CBRS Radio Sites", [],
    multiValue=True,
    min=0,
    multiValueTitle="CBRS X310 radios to allocate.",
    members=[
        portal.Parameter(
            "radio",
            "CBRS Radio Site",
            portal.ParameterType.STRING,
            cbrs_radios[0], cbrs_radios,
            longDescription="CBRS X310 radio will be allocated from selected site."
        ),
    ])


portal.context.defineParameter("alloc_shuttles", 
                               "Allocate all routes (mobile endpoints)",
                               portal.ParameterType.BOOLEAN, False)

portal.context.defineParameter("start_vnc", 
                               "Start X11 VNC on all compute nodes",
                               portal.ParameterType.BOOLEAN, True)

# Bind parameters
params = portal.context.bindParameters()

# Get request object
request = portal.context.makeRequestRSpec()

# X310 radio + node pair allocation helper routine
def x310_node_pair(x310_component_id, node_type):
    radio_link = request.Link("%s-link" % x310_component_id)
    node = request.RawPC("%s-node" % x310_component_id)
    node.hardware_type = node_type
    node.disk_image = x310_node_disk_image
    node_radio_if = node.addInterface("usrp_if")
    node_radio_if.addAddress(rspec.IPv4Address("192.168.40.1",
                                               "255.255.255.0"))
    radio_link.addInterface(node_radio_if)
    radio = request.RawPC("%s-radio"% x310_component_id)
    radio.component_id = x310_component_id
    radio_link.addNode(radio)

    node.addService(rspec.Execute(shell="bash",
                                  command = "sudo patch /usr/bin/uhd_fft < /local/repository/uhd_fft.patch"))
    node.addService(rspec.Execute(shell="bash",
                                  command = "sudo patch /usr/lib/python3/dist-packages/gnuradio/uhd/uhd_siggen_base.py < /local/repository/uhd_siggen.patch"))
    
    node.addService(rspec.Execute(shell="bash",
                                  command = "sudo cp /local/repository/tx_samples_from_file_patch ~"))
    
    node.addService(rspec.Execute(shell="bash",
                                  command="sudo chmod +x /local/repository/setup_vnc.sh"))
    node.addService(rspec.Execute(shell="bash",
                                  command="sudo /local/repository/setup_vnc.sh"))

    node.addService(rspec.Execute(shell="bash",
                                  command="sudo chmod +x /local/repository/start_vnc.sh"))
    node.addService(rspec.Execute(shell="bash",
                                  command="sudo /local/repository/start_vnc.sh"))



    if params.start_vnc:
        node.startVNC(True)

# Declare that we may be starting X11 VNC on the compute nodes.
if params.start_vnc:
    request.initVNC()

# Request frequency range(s)
for frange in params.freq_ranges:
    request.requestSpectrum(frange.freq_min, frange.freq_max, 0)

# Request PC + CBRS radio resource pairs.
for rsite in params.cbrs_radio_sites:
    x310_node_pair(rsite.radio, params.x310_comp_nodetype)

portal.context.printRequestRSpec()
